#ifndef ARM_H
#define ARM_H

#define ARM_NREGS 16    /* # of registers available */

#define ARM_PC_INC 8    /* amount the PC is incremented by */

#define ARM_IP 12       /* something */
#define ARM_SP 13       /* stack pointer */
#define ARM_LR 14       /* link register */
#define ARM_PC 15       /* program counter */

#define ARM_WORD     4 /* bytes */
#define ARM_HALFWORD 2 /* bytes */
#define ARM_BYTE     1 /* byte */

#define ARM_WORD_BITS     ARM_WORD * 8      /* bits in a word */
#define ARM_HALFWORD_BITS ARM_HALFWORD * 8  /* bits in a halfword */
#define ARM_BYTE_BITS     ARM_BYTE * 8      /* bits in a byte */


#define ARM_WORD_ALIGNED 0x00000003         /* mask to check word alignment */
#define ARM_HALFWORD_ALIGNED 0x00000001     /* mask to check haflword align- */

#define ARM_WIDTH ARM_WORD_BITS             /* instruction width is one word */


#define COND_MASK           (0xF << (ARM_WIDTH - 4))  /* condition field */
#define ITYPE_MASK          (0x3 << (ARM_WIDTH - 6))  /* instruction type */

#define IT_BRANCH_BITS      (0x5 << (ARM_WIDTH - 7))  /* branch opcode */
#define IT_LOADSTORE_BITS   (0xA << (ARM_WIDTH - 6))  /* load/store opcode */


#define BRANCH_LINK         (0x1 << (ARM_WIDTH - 8))  /* [branch] and link */
#define BRANCH_IMM_MASK     0x00FFFFFF                /* 24 bit immediate */
#define BRANCH_IMM_SIZE     (ARM_WIDTH - 8)

#define BIT(x)              (0x1 << (x))
#define MSB(x)              (((x) >> 31) & 0x1)
#define LSB(x)              ((x) & 0x1)

#define ARM_NOP             0xE1A00000

#define DATA_OPC(x)         (((x) >> 21) & 0xF)

#define OPC_AND   0x0              /* 0000 */
#define OPC_EOR   0x1              /* 0001 */
#define OPC_SUB   0x2              /* 0010 */
#define OPC_RSB   0x3              /* 0011 */
#define OPC_ADD   0x4              /* 0100 */
#define OPC_ADC   0x5              /* 0101 */
#define OPC_SBC   0x6              /* 0110 */
#define OPC_RSC   0x7              /* 0111 */
#define OPC_TST   0x8              /* 1000 */
#define OPC_TEQ   0x9              /* 1001 */
#define OPC_CMP   0xA              /* 1010 */
#define OPC_CMN   0xB              /* 1011 */
#define OPC_ORR   0xC              /* 1100 */
#define OPC_MOV   0xD              /* 1101 */
#define OPC_BIC   0xE              /* 1110 */
#define OPC_MVN   0xF              /* 1111 */

#define MUL_OPC(x)          (((x) >> 4) & 0xF)
#define OPC_MUL   0x9              /* 1001 */

    /* TODO: test opcodes */
#define OPC_MRS   (0xF << (ARM_WIDTH - 16)) | (0x1 << (ARM_WIDTH - 8)))
#define OPC_MSR   (0x128F000)                            /* 00010 0 10 1000(field F) 1111 */
#define OPC_LDR   (((0x1 << (ARM_WIDTH - 6)) | (0x1 << (ARM_WIDTH - 12)))
#define OPC_STR   (0x1 << (ARM_WIDTH - 6))



#define ARM_REG_MASK 0xF

/* DATA register offset */
#define ARM_DATA_RD_OFFSET 12
#define ARM_DATA_RN_OFFSET 16
#define ARM_DATA_RM_OFFSET 0 
#define ARM_DATA_RS_OFFSET 8 

/* DATA register mask */
#define ARM_DATA_RD          (ARM_REG_MASK << ARM_DATA_RD_OFFSET)
#define ARM_DATA_RN          (ARM_REG_MASK << ARM_DATA_RN_OFFSET)
#define ARM_DATA_RM          (ARM_REG_MASK << ARM_DATA_RM_OFFSET)
#define ARM_DATA_RS          (ARM_REG_MASK << ARM_DATA_RS_OFFSET)

/* DATA extract the register number from instruction x */
#define REG_DATA_RD(x)         ( ((x) & ARM_DATA_RD) >> ARM_DATA_RD_OFFSET)
#define REG_DATA_RN(x)         ( ((x) & ARM_DATA_RN) >> ARM_DATA_RN_OFFSET)
#define REG_DATA_RM(x)         ( ((x) & ARM_DATA_RM) >> ARM_DATA_RM_OFFSET)
#define REG_DATA_RS(x)         ( ((x) & ARM_DATA_RS) >> ARM_DATA_RS_OFFSET)

/* MUL register offset */
#define ARM_MUL_RD_OFFSET 16
#define ARM_MUL_RN_OFFSET 12
#define ARM_MUL_RM_OFFSET 0 
#define ARM_MUL_RS_OFFSET 8 

/* MUL register mask */
#define ARM_MUL_RD          (ARM_REG_MASK << ARM_MUL_RD_OFFSET)
#define ARM_MUL_RN          (ARM_REG_MASK << ARM_MUL_RN_OFFSET)
#define ARM_MUL_RM          (ARM_REG_MASK << ARM_MUL_RM_OFFSET)
#define ARM_MUL_RS          (ARM_REG_MASK << ARM_MUL_RS_OFFSET)

/* MUL extract the register number from instruction x */
#define REG_MUL_RD(x)         ( ((x) & ARM_MUL_RD) >> ARM_MUL_RD_OFFSET)
#define REG_MUL_RN(x)         ( ((x) & ARM_MUL_RN) >> ARM_MUL_RN_OFFSET)
#define REG_MUL_RM(x)         ( ((x) & ARM_MUL_RM) >> ARM_MUL_RM_OFFSET)
#define REG_MUL_RS(x)         ( ((x) & ARM_MUL_RS) >> ARM_MUL_RS_OFFSET)

#define SAVE                BIT(20)

/* CPSR */
#define N_FLAG              BIT(31)
#define Z_FLAG              BIT(30)
#define C_FLAG_OFFSET       29
#define C_FLAG              BIT(C_FLAG_OFFSET)
#define V_FLAG_OFFSET       28
#define V_FLAG              BIT(V_FLAG_OFFSET)

#define SHIFT_MASK          0x3
#define SHIFT_MASK_OFFSET   0x5
#define SHIFT_TYPE(x)       ((x) & (SHIFT_MASK << SHIFT_MASK_OFFSET))

#define SHIFT_IMM8_MASK     0xFF
#define SHIFT_ROT_IMM(x)    (((x) >> 8) & 0xF)

#define LSL                 0x0
#define LSR                 0x1
#define ASR                 0x2
#define ROR                 0x3

#define FLEXOP2_REG_MASK            0xE000FF0
#define FLEXOP2_SHIFT_OP(x)         (((x) >> 5) & 0x3)
#define FLEXOP2_SHIFT_IMM(x)        (((x) >> 7) & 0xF)

/* Sign-extends a value based on the bitwidth */
unsigned signextend(unsigned value, unsigned bitwidth);

/** Rotates value left by the specified amount */
unsigned int rotatel(unsigned int value, signed amount);

/** Rotates a value right by the specified amount */
unsigned int rotater(unsigned int value, signed amount);


#endif

