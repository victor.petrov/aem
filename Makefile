TARGET:=aem
CC:=gcc
CFLAGS:=-Wall -pedantic -Werror
CHMOD:=chmod

ifdef DEBUG
CFLAGS+=-ggdb -DDEBUG
else
CFLAGS+=-DNDEBUG -O2
endif

.PHONY: test

all: ${TARGET}

${TARGET}: ${TARGET}.o util.o mem.o mif.o cpu.o i.o arm.o alu.o shifter.o mul.o
	${CC} ${CFLAGS} -o "$@" $+
	${CHMOD} +x $@

${TARGET}.o: ${TARGET}.c ${TARGET}.h cpu.h mem.h mif.h

arm.o: arm.c arm.h util.h

cpu.o: cpu.c cpu.h arm.h mem.h util.h

alu.o: alu.c alu.h arm.h util.h

shifter.o: shifter.c shifter.h arm.h util.h

mul.o: mul.c mul.h arm.h util.h

i.o: i.c i.h arm.h util.h

mem.o: mem.c mem.h mif.h util.h arm.h

mif.o: mif.c mif.h util.h

util.o: util.c util.h

debug:
	@make --no-print-directory clean
	@make --no-print-directory DEBUG=1

clean:
	rm -f ${TARGET} *.o

test: ${TARGET}
	cd test && make && cd ..
	./${TARGET} test/pt.mif
