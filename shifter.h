#ifndef SHIFTER_H
#define SHIFTER_H

typedef enum TShiftType
{
	SHIFT_NONE,
	SHIFT_IMM,
	SHIFT_REG_IMM,
	SHIFT_REG_REG
} ShiftType;

typedef struct TShifter
{
	unsigned op;
	unsigned carry_out;	
	unsigned cpsr_carry;	/* whether carry_out should be set to C_FLAG */
	ShiftType type;

    unsigned amount;		/* number of bits to shift / rotate */
    unsigned value;      /* mode 1 immediate (8 bit) */
} Shifter;

Shifter* Shifter_new();
unsigned Shifter_exec(Shifter* S, unsigned carry_in);
#endif

