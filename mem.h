#ifndef MEM_H
#define MEM_H

#include <stddef.h>
#include "mif.h"

typedef struct TMEM
{
	unsigned size;			/* size of memory */

	union
	{
		unsigned* w;		/* mem as words */
		unsigned short* h;	/* mem as halfwords */
		unsigned char* b;	/* mem as bytes */
	} data;
} MEM;

MEM* MEM_new();
MEM* MEM_fromMIF(const MIF* mif);
void MEM_loadMIF(MEM* mem, const MIF* mif);

/* Performs a memory read */
unsigned short MEM_read(MEM* mem, unsigned address);
#endif

