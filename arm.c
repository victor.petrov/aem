#include "arm.h"

#include <stdio.h>

/* Sign-extends a value based on the bitwidth */
unsigned signextend(unsigned value, unsigned bitwidth)
{
	unsigned result=0;
	unsigned shift=ARM_WIDTH-bitwidth;

    /* the convoluted expression below will shift the value to the left until 
       its bits are left aligned, then it will take the MSB and make it LSB 
       (so the mask is either 1 or 0 at this point). Then 1 or 0 becomes 
       -1 or 0, by negating and adding 1.
       Then the value is AND-ed with the mask, to obtain the 
       sign-extended value */
    result=value << shift;
    result&=BIT(31);
    result>>=31;
    result=~result;
    result+=1;
    result<<=bitwidth;
    result|=value;

	return result;
}

/** Rotates a value left by the specified amount */
unsigned int rotatel(unsigned value, signed amount)
{
	return (value<<amount) | (value>>(ARM_WIDTH-amount)); 
}

/** Rotates a value right by the specified amount */
unsigned int rotater(unsigned value, signed amount)
{
	return (value>>amount) | (value<<(ARM_WIDTH-amount)); 
}

