#ifndef UTIL_H
#define UTIL_H

#include <stddef.h>

#define CHECK(x,msg) { if (!(x)) { perror(msg); exit(EXIT_FAILURE); } }

#define strequals(x,y) (0==strcmp((x),(y)))

/* Duplicates a string (strdup) */
char* strduplicate(const char* source, size_t length);

/* Displays an error about memory allocation failure and exits */
void memerror(size_t bytes, int err);

/* Displays an error about file open error and exits */
void fileerror(const char* file, const char* action, int err);

/* Trims whitespace from the beginning and end of string */
void trim(char* str);

/* Converts octal/dec/hex number string to unsigned int
* code needs to contain a pointer to the start of the string,
* such as '007' or 'FFFF'
*/
unsigned toUInt(const char* num, int base);

#endif

