#include "cpu.h"

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "arm.h"
#include "util.h"

/* Decodes data instructions */
static void CPU_decodeData(CPU* cpu);

/* Decodes mode 1 address mode */
static void CPU_decodeMode1(CPU* cpu);

/* Decodes branch instructions */
static void CPU_decodeLoadStore(CPU* cpu);

/* Decodes branch instructions */
static void CPU_decodeBranch(CPU* cpu);

/* Decodes multiply instructions */
static void CPU_decodeMultiply(CPU* cpu);

/* Executes a branch instruction */
static void CPU_execBranch(CPU* cpu);

/* Executes a load/store instruction */
static void CPU_execLoadStore(CPU* cpu);

/* Executes a data processing instruction */
static void CPU_execData(CPU* cpu);

/* Allocates memory for a new ARM CPU */
CPU* CPU_new(MEM* mem)
{
    CPU* result=NULL;

    assert(mem);

    result=malloc(sizeof(*result));

    if (!result)
        memerror(sizeof(*result),errno);

    result->regs=calloc(1,sizeof(*(result->regs)));
    if (!result->regs)
        memerror(sizeof(*result->regs),errno);

    result->regs->r=malloc(ARM_NREGS * sizeof(unsigned));
    if (!result->regs->r)
        memerror(ARM_NREGS * sizeof(unsigned), errno);

    result->regs->count=ARM_NREGS;
    result->regs->dirty=0;
    result->ram=mem;

    result->alu=ALU_new(&result->cpsr);

    result->shifter=Shifter_new();

    result->mul=MUL_new();

    return result;
}

/* Initializes the CPU */
void CPU_init(CPU* cpu)
{
    assert(cpu);
    assert(cpu->regs);
    assert(cpu->regs->r);
    assert(cpu->regs->count);

    REG(cpu,ARM_PC)=0;  /* set PC = 0 */
    cpu->cpsr=0;        /* set CPSR = 0, clear flags */
    cpu->i1=cpu->i2=0;

    /* read first instruction */
    CPU_fetch(cpu);
}

/* Fetches an instruction */
void CPU_fetch(CPU* cpu)
{
    unsigned short hw=0;
    unsigned address=0;
    unsigned result=0;

    assert(cpu);
    assert(cpu->ram);
    assert(cpu->regs);

    /* Read address from PC */
    address=cpu->regs->r[ARM_PC];

    /* Read first half-word at the address */
    hw=MEM_read(cpu->ram,address);
    result|=hw;

    /* Read second halfword at the address */

    hw=MEM_read(cpu->ram, address + ARM_HALFWORD);
    result|=(hw << ARM_HALFWORD_BITS);

    /* Place the previous instruction in i1, to be executed */
    cpu->i1=cpu->i2;
    /* Place the current instruction in i2, in the queue */
    cpu->i2=result;

/*    printf("next instruction   : %08x\n",cpu->i2);
    printf("current instruction: %08x\n",cpu->i1);
*/
    cpu->regs->r[ARM_PC]+=ARM_WORD;
}

/* Decodes an instruction */
void CPU_decode(CPU* cpu)
{
    assert(cpu);
    assert(cpu->alu);

    /* turn off all control lines */
    cpu->save_flags=0;
    /* turn on all individual flags */
    cpu->c_from=cpu->v_from=FLAGSRC_ALU;
    cpu->multiply=0;
    cpu->link=0;
    cpu->reg_write=0;
    cpu->shifter->cpsr_carry=0;
    cpu->shifter->type=SHIFT_NONE;

    cpu->i=I_new(cpu->i1);

    /* get the condition field */
    cpu->i->cond = cpu->i1 & COND_MASK;

    /* get the instruction type (preemptively).
      there will still be some undefined instructions, even if at
      this point the type will be set to IT_DATA (for example),
      but those need more bit-checking and will be set to IT_UNDEFINED
      by their specialized decoder functions. */
    cpu->i->type = (cpu->i1 & ITYPE_MASK) >> (ARM_WIDTH - 6);

    switch (cpu->i->type)
    {
        case IT_DATA:        CPU_decodeData(cpu);         break;
        case IT_LOADSTORE:   CPU_decodeLoadStore(cpu);    break;
        case IT_BRANCH:      CPU_decodeBranch(cpu);       break;
        case IT_UNDEFINED:   break;
    }

    if (cpu->i->type==IT_UNDEFINED)
    {
        fprintf(stderr,"cpu error: undefined instruction 0x%x\n",cpu->i1);
        exit(EXIT_FAILURE);
    }
}


/* Decodes data instructions */
static void CPU_decodeData(CPU* cpu)
{
    unsigned bits=0;

    assert(cpu);
    assert(cpu->i);

    bits=cpu->i->bits;

    if ((bits & BIT(25)) && (bits & BIT(24)) && !(bits & BIT(21))) 
    {
        cpu->i->type=IT_UNDEFINED;
        return;
    }

    cpu->i->type=IT_DATA;

    if (bits & SAVE)
        cpu->save_flags=1;

    /* multiply? */
    if (OPC_MUL == MUL_OPC(bits))
    {
        CPU_decodeMultiply(cpu);
        return;
    }

    cpu->regs->rd=REG_DATA_RD(bits);
    cpu->regs->rn=REG_DATA_RN(bits);
    cpu->regs->rs=REG_DATA_RS(bits);
    cpu->regs->rm=REG_DATA_RM(bits);

    cpu->alu->op=DATA_OPC(bits);

    /* opc specific */
    switch (cpu->alu->op)
    {
        /* arithmetic */
        case OPC_ADC:
        case OPC_ADD:
        case OPC_RSB:
        case OPC_RSC:
        case OPC_SUB:
        case OPC_SBC:
                      cpu->reg_write=1;
                      break;
        /* logical */
        case OPC_AND:
        case OPC_BIC:
        case OPC_EOR:
        case OPC_ORR:
        case OPC_MOV: 
        case OPC_MVN: cpu->c_from=FLAGSRC_SHIFTER;
                      cpu->v_from=FLAGSRC_NONE;
                      cpu->reg_write=1; /* store result in rd */
                      break;

        /* leave Rd intact */
        case OPC_CMN:
        case OPC_CMP:
        case OPC_TEQ:
        case OPC_TST:
                      cpu->reg_write=0; /* do not store result in rd */
                      break;
    }

    I_printOp(LOC(cpu),cpu->i,cpu->alu->op,cpu->save_flags,
              cpu->regs->rd,cpu->regs->rn);

    /* Decode FlexOp2 p.A5-6 */
    CPU_decodeMode1(cpu);
}

static void CPU_decodeMode1(CPU* cpu)
{
    unsigned bits=0;
    unsigned immediate=0;

    assert(cpu);
    assert(cpu->alu);
    assert(cpu->regs);
    assert(cpu->shifter);

    bits=cpu->i->bits;

    /* immediate */
    immediate=bits & BIT(25);

    /* 1) IMM p.A5-6 */
    if (immediate)
    {
        cpu->shifter->op=ROR;
        cpu->shifter->amount=SHIFT_ROT_IMM(bits) << 1;
        cpu->shifter->value=(bits & SHIFT_IMM8_MASK);
        cpu->shifter->type=SHIFT_IMM;

        if (!cpu->shifter->amount)
            cpu->shifter->cpsr_carry=1;

        I_printMode1Immediate(rotater(cpu->shifter->value,
                                      cpu->shifter->amount));
    }
    else
    /* 2) REG p.A5-8 (technically encoded as REG LSL #0 ) */
    if (0==(bits & FLEXOP2_REG_MASK))
    {
        cpu->shifter->op=LSL;
        cpu->shifter->type=SHIFT_REG_IMM;
        cpu->shifter->amount=0;

        I_printMode1Reg(cpu->regs->rm);
    }
    else
    {
        /* 3) REG shift_op REG */
        if (bits & BIT(4))
        {
            cpu->shifter->op=FLEXOP2_SHIFT_OP(bits);
            cpu->shifter->type=SHIFT_REG_REG;

            I_printMode1RegShiftReg(cpu->regs->rm, 
                                    cpu->shifter->op,
                                    cpu->regs->rs);
        }
        else
        /* REG shift_op #imm */
        {
            cpu->shifter->op=FLEXOP2_SHIFT_OP(bits);
            cpu->shifter->type=SHIFT_REG_IMM;
            cpu->shifter->amount=FLEXOP2_SHIFT_IMM(bits);

            I_printMode1RegShiftImm(cpu->regs->rm,
                                    cpu->shifter->op,
                                    cpu->shifter->amount);
        }
    }
}

static void CPU_decodeMultiply(CPU* cpu)
{
    unsigned bits=0;
    unsigned accumulate=0;

    assert(cpu);
    assert(cpu->i);

    bits=cpu->i->bits;

    
    if ((bits & BIT(24)) || /* swap? */
        (bits & BIT(23))   /* multiply long? */
       )
    {
        cpu->i->type=IT_UNDEFINED;
        return;
    }

    cpu->regs->rd=REG_MUL_RD(bits);
    cpu->regs->rn=REG_MUL_RN(bits);
    cpu->regs->rs=REG_MUL_RS(bits);
    cpu->regs->rm=REG_MUL_RM(bits);

    accumulate=bits & BIT(21);

    if (accumulate)
    {
        cpu->alu->op=OPC_ADD;       /* accumulate */
        cpu->c_from=FLAGSRC_NONE;   /* p. A4-55 */
        cpu->v_from=FLAGSRC_NONE;
    }

    cpu->multiply=1;

    cpu->reg_write=1; /* store result in rd */

    I_printMultiply(LOC(cpu),cpu->i,accumulate,cpu->save_flags,
                    cpu->regs->rd, cpu->regs->rs, cpu->regs->rm, cpu->regs->rn);
}

/* Decodes branch instructions */
static void CPU_decodeLoadStore(CPU* cpu)
{
    printf("[CPU] STUB: %s\n",__FUNCTION__);
}

/* Decodes branch instructions:
   bits [27:26] ="10". */
static void CPU_decodeBranch(CPU* cpu)
{
    /* mark the undefined instructions at start */
    if ((cpu->i->cond==COND_NV) || /* 1111|10x... */
        !(cpu->i->bits & BIT(25))) /* xxxx|100... */
    {
        cpu->i->type=IT_UNDEFINED;
        return;
    }

    cpu->link=!(!(cpu->i->bits & BRANCH_LINK));
    /* p. A4-10 */
    cpu->i->x.b.offset=REG(cpu,ARM_PC) + 
                          (signextend(cpu->i->bits & BRANCH_IMM_MASK,
                                     BRANCH_IMM_SIZE) << 2);

    I_printBranch(LOC(cpu),cpu->i,cpu->link);
}


void CPU_go(CPU* cpu)
{
    assert(cpu);

    while ((getchar()!='q'))
    {

        /* Fetch next instruction */
        CPU_fetch(cpu);

        /* Decode current instruction */
        CPU_decode(cpu); 

        /* Execute current instruction */
        CPU_exec(cpu);

        /* Prints current register values */
        CPU_printRegs(cpu);
    }
}

void CPU_exec(CPU* cpu)
{
    assert(cpu);
    assert(cpu->i);
    assert(cpu->regs);

    cpu->regs->dirty=0;

    if (cpu->i->bits==ARM_NOP)
        return;

    switch (cpu->i->type)
    {
        case IT_BRANCH:     CPU_execBranch(cpu);    break;
        case IT_LOADSTORE:  CPU_execLoadStore(cpu); break;
        case IT_DATA:       CPU_execData(cpu);      break;
        case IT_UNDEFINED:  fprintf(stderr,"cpu exec error: cannot execute "
                                    "an undefined instruction: 0x%x\n",
                                    cpu->i->bits);  break;
    }
}

void CPU_execBranch(CPU* cpu)
{
    assert(cpu);

    cpu->i2=ARM_NOP;
    /* write to LR the address after the B instruction */
    if (cpu->link)
        REG_WRITE(cpu,ARM_LR,REG(cpu,ARM_PC)-ARM_WORD);
    REG_WRITE(cpu,ARM_PC,cpu->i->x.b.offset);
}

void CPU_execLoadStore(CPU* cpu)
{
    printf("[EXEC] STUB/LOADSTORE: 0x%x\n",cpu->i->bits);
}

void CPU_execData(CPU* cpu)
{
    unsigned alu_input2=0;

    unsigned mul_input1=0;
    unsigned mul_input2=0;

    unsigned alu_result=0;

    assert(cpu);
    assert(cpu->regs);
    assert(cpu->alu);
    assert(cpu->shifter);
    assert(cpu->mul);

    /* alu input 1 */
    cpu->alu->a=REG(cpu,cpu->regs->rn);

    /* multiplier input 1 and 2 */
    mul_input1=REG(cpu,cpu->regs->rm);
    mul_input2=REG(cpu,cpu->regs->rs);

    /* either multiply or shift */
    if (cpu->multiply)
        alu_input2=MUL_exec(cpu->mul);
    else
    {
        if (cpu->shifter->type==SHIFT_REG_IMM ||
            cpu->shifter->type==SHIFT_REG_REG)
        {
            cpu->shifter->value=REG(cpu,cpu->regs->rm);

        }

        if (cpu->shifter->type==SHIFT_REG_REG)
                /* least significant byte of Rs, p.A5-10 */
                cpu->shifter->amount=REG(cpu,cpu->regs->rs) & 0xFF; 

        alu_input2=Shifter_exec(cpu->shifter,
                               (cpu->cpsr & C_FLAG) >> C_FLAG_OFFSET);
    }
    
    /* alu input 2 */
    cpu->alu->b=alu_input2;

    /* do ALU operation, pass in the carry flag */
    alu_result=ALU_exec(cpu->alu,
                        (cpu->cpsr & C_FLAG) >> C_FLAG_OFFSET, 
                        cpu->shifter->carry_out); 

    /* save flags into CPSR */
    if (cpu->save_flags)
    {
        /* N */
        cpu->cpsr&=~N_FLAG;
        cpu->cpsr|=cpu->alu->flags & N_FLAG;

        /* Z */
        cpu->cpsr&=~Z_FLAG;
        cpu->cpsr|=cpu->alu->flags & Z_FLAG;

        /* C */
        if (cpu->c_from==FLAGSRC_ALU)
        {
            cpu->cpsr&=~C_FLAG;
            cpu->cpsr|=cpu->alu->flags & C_FLAG;
        }
        else
        if (cpu->c_from==FLAGSRC_SHIFTER)
        {
            cpu->cpsr&=~C_FLAG;
            cpu->cpsr|=(cpu->shifter->carry_out & 0x1) << C_FLAG_OFFSET;
        }

        /* V */
        if (cpu->v_from!=FLAGSRC_NONE)
        {
            cpu->cpsr&=~V_FLAG;
            cpu->cpsr|=cpu->alu->flags & V_FLAG;
        }
    }

    /* reg write? */
    if (cpu->reg_write)
        REG_WRITE(cpu,cpu->regs->rd,alu_result);
}

void CPU_printRegs(CPU* cpu)
{
    unsigned i=0;
    unsigned int changed=0;
    assert(cpu);
    assert(cpu->regs);
    assert(cpu->regs->r);
    assert(cpu->regs->count);

    printf("\n");

    for (i=0;i<cpu->regs->count;++i)
    {
        changed=cpu->regs->dirty & BIT(i);

        printf("%sR%02u: %08x%s  ", changed?"\033[0;32m":"",
               i,REG(cpu,i),        changed?"\033[0m":"");
        if (!((i+1)%4))
            printf("\n");
    }
    printf("CPSR: %s%s%s%s\n",
           (cpu->cpsr & N_FLAG)?"N":"-",
           (cpu->cpsr & Z_FLAG)?"Z":"-",
           (cpu->cpsr & C_FLAG)?"C":"-",
           (cpu->cpsr & V_FLAG)?"V":"-");

    printf("\n");

}


