#ifndef CPU_H
#define CPU_H

#include "alu.h"
#include "mem.h"
#include "shifter.h"
#include "mul.h"
#include "i.h"

#define REG(c,x)          ((c)->regs->r[(x)])
#define REG_WRITE(c,x,y)  {                                 \
                            if (REG(c,x)!=y)                \
                            {                               \
                                REG((c),(x))=(y);           \
                                c->regs->dirty|=BIT((x));   \
                            }                               \
                          }

#define LOC(c)            (REG((c),ARM_PC)-ARM_PC_INC)

typedef enum TFlagSrc
{
    FLAGSRC_NONE,
    FLAGSRC_SHIFTER,
    FLAGSRC_ALU
} FlagSrc;

typedef struct TRegisters
{
    unsigned count;
    unsigned* r;            /* the register bank */
    unsigned dirty;         /* registers that have been changed */

    unsigned rd;
    unsigned rn;
    unsigned rs;
    unsigned rm;
} Registers;

typedef struct TCPU
{
    I*          i;          /* current decoded instruction */
    unsigned    i1;         /* current instruction */
    unsigned    i2;         /* next instruction */
    unsigned    cpsr;       /* flags */
    Registers*  regs;       /* registers */
    MEM*        ram;        /* ram */
    ALU*        alu;        /* the mighty ALU */
    Shifter*    shifter;    /* the shifter */
    MUL*        mul;        /* the multiplier */

    /* control lines */
    unsigned    link;       /* save PC to LR ? */
    unsigned    save_flags; /* save flags to CPSR? */
    unsigned    c_from;     /* all other flags are set by the ALU */
    unsigned    v_from;     /* all other flags are set by the ALU */

    unsigned    multiply;   /* multiply? otherwise, shift */
    unsigned    reg_write;  /* store result in rd? */
} CPU;

CPU* CPU_new(MEM* mem);
void CPU_init(CPU* cpu);
void CPU_go(CPU* cpu);

void CPU_fetch(CPU* cpu);
void CPU_decode(CPU* cpu);
void CPU_exec(CPU* cpu);
void CPU_printRegs(CPU* cpu);
#endif

