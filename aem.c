#include <stdio.h>
#include <stdlib.h>

#include "aem.h"
#include "mif.h"
#include "mem.h"
#include "cpu.h"

/* main */
int main(int argc, char** argv)
{
    CPU*  arm=NULL;
    MEM*  mem=NULL; 
    MIF*  mif=NULL;
    
    /* check arguments */
    if (argc<2)
    {
        showUsage(argv[0]);
        exit(EXIT_FAILURE);
    }

    /* load MIF data from file */
    mif=MIF_load(argv[1]);

    /* validate the config */
    MIF_validate(mif);
    
    printf("width: %u\ndepth:%u\naddr:%u\ndata:%u\n",
           mif->width,mif->depth,
           mif->addr_radix,mif->data_radix);

    /* Create a 2MB memory image */
    mem=MEM_fromMIF(mif);

    /* Create a new ARM CPU */
    arm=CPU_new(mem);

    /* Initialize the CPU */
    CPU_init(arm);

    printf("\nPress <ENTER> to execute one instruction (repeat for each instruction)\n");

    /* Run! */
    CPU_go(arm);

    printf("0x%x\n",signextend((unsigned)((2-9) & BRANCH_IMM_MASK),24));

    return EXIT_SUCCESS;
}

void showUsage(const char* argv0)
{
    printf("Victor's ARMv5 Emulator v.0.1\n\n");
    printf("Usage: %s <file.mif>\n",argv0);
}
