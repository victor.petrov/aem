#include "mem.h"

#include <errno.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "arm.h"
#include "util.h"

#define MEMSIZE 2*1024*1024 /* 2MB */

/* Allocates a new chunk of memory, based on MEMSIZE */
MEM* MEM_new()
{
    MEM* result=NULL;

    result=malloc(sizeof(*result));

    if (!result)
        memerror(sizeof(*result),errno);

    result->data.b=malloc(MEMSIZE);

    if (!result)
        memerror(MEMSIZE,errno);

    result->size=MEMSIZE;

    return result;

}

/* Copies the data loaded into a MIF object into the MEM data */
void MEM_loadMIF(MEM* mem, const MIF* mif)
{
    assert(mem);
    assert(mif);
    
    if (mif->depth>mem->size)
    {
        fprintf(stderr,"%s: DEPTH value exceeds VM size (%u bytes)\n",
                mif->filename,mem->size);
        exit(EXIT_FAILURE);
    }

    /* copy the data */
    memcpy(mem->data.b,mif->data,mif->depth*(mif->width/8));
    
    printf("copied %u bytes to MEM\n",mif->depth*(mif->width/8));
}

/* Creates a new chunk of memory and loads a MIF file into it */
MEM* MEM_fromMIF(const MIF* mif)
{
    MEM* result=NULL;

    assert(mif);

    result=MEM_new();

    MEM_loadMIF(result,mif);

    return result;
}


/* Performs a memory read */
unsigned short MEM_read(MEM* mem, unsigned address)
{
    unsigned short result=0;

    assert(mem);

    /* is the address aligned properly ? */
    if (address & ARM_HALFWORD_ALIGNED)
    {
        fprintf(stderr,"memory read error: address 0x%x is not aligned\n", 
                address);
        exit(EXIT_FAILURE);
    }

    /* is the address greater than the size of the memory? */
    if (address >= mem->size - ARM_HALFWORD)
    {
        fprintf(stderr,"memory read error: address 0x%x is greater than the "
               "total available memory (%u bytes)\n",address,mem->size);
    }

    result|=mem->data.b[address];
    result|=(mem->data.b[address+1] << ARM_BYTE_BITS);

    return result;
}
