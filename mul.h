#ifndef MUL_H
#define MUL_H

typedef struct TMUL
{
	unsigned a;
	unsigned b;

	unsigned flags;

} MUL;

MUL* MUL_new();
unsigned MUL_exec(MUL* mul);

#endif

