#include "alu.h"

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "arm.h"
#include "util.h"

ALU* ALU_new()
{
    ALU* result=NULL;

    result=malloc(sizeof(*result));

    if (!result)
        memerror(sizeof(*result),errno);

    return result;
}

/* Performs an ALU operation. Carry/Overflow are computed based on:
http://teaching.idallen.com/dat2343/10f/notes/040_overflow.txt
*/
unsigned ALU_exec(ALU* alu, unsigned carry_in, unsigned shifter_carry_out)
{
    unsigned result=0;
	unsigned carry_out=0;
	unsigned overflow=0;

    assert(alu);

    alu->op_type=ALU_OP_UNDEFINED;
	carry_in&=1;					/* make sure only LSB is used */

    switch (alu->op)
    {
		case OPC_ADC:	alu->op_type=ALU_OP_ARITHMETIC;
						result = alu->a + alu->b + carry_in;
						break;

        case OPC_ADD:   alu->op_type=ALU_OP_ARITHMETIC;
                        result = alu->a + alu->b; 
						carry_out=(result < alu->a);

						/* overflow=(a xnor b) && (a xor c) */
						overflow=(MSB(alu->a) == MSB(alu->b)) && (MSB(alu->a) != MSB(result)); /* add */ 
                        break;

        case OPC_AND:   alu->op_type=ALU_OP_LOGICAL;    /* no carry out */
                        result = alu->a & alu->b;
                        break;

        case OPC_BIC:   alu->op_type=ALU_OP_LOGICAL;    /* no carry out */
                        result = alu->a & (~alu->b);
                        break;

        case OPC_CMN:   alu->op_type=ALU_OP_ARITHMETIC;
                        result = alu->a + alu->b;		/* p. A4-23 */
						carry_out=(result < alu->a);
						overflow=(MSB(alu->a) == MSB(alu->b)) && (MSB(alu->a) != MSB(result)); /* add */ 
                        break;

        case OPC_CMP:   alu->op_type=ALU_OP_ARITHMETIC;
                        result = alu->a - alu->b;		/* p. A4-23 */
                        break;

        case OPC_EOR:   alu->op_type=ALU_OP_LOGICAL;    /* no carry out */
                        result = alu->a ^ alu->b;
                        break;

        case OPC_ORR:   alu->op_type=ALU_OP_LOGICAL;    /* no carry out */
                        result = alu->a | alu->b;
                        break;

        case OPC_RSB:   alu->op_type=ALU_OP_ARITHMETIC;	/* reverse subtract */
                        result = alu->b - alu->a;
						carry_out=(result > alu->b);
			  			overflow=(MSB(alu->b) != MSB(alu->a)) && (MSB(alu->a) == MSB(result)); /* sub */
                        break;

        case OPC_RSC:   alu->op_type=ALU_OP_ARITHMETIC;	/* reverse subtract */
                        result = alu->b - alu->a - ~carry_in;
						carry_out=(result > alu->b);
			  			overflow=(MSB(alu->b) != MSB(alu->a)) && (MSB(alu->a) == MSB(result)); /* sub */
                        break;

        case OPC_SUB:   alu->op_type=ALU_OP_ARITHMETIC;
                        result = alu->a - alu->b;
						carry_out=(result > alu->a);
			  			overflow=(MSB(alu->a) != MSB(alu->b)) && (MSB(alu->b) == MSB(result)); /* sub */
                        break;

        case OPC_SBC:   alu->op_type=ALU_OP_ARITHMETIC;
                        result = alu->a - alu->b - ~carry_in;
						carry_out=(result > alu->a);
			  			overflow=(MSB(alu->a) != MSB(alu->b)) && (MSB(alu->b) == MSB(result)); /* sub */
                        break;

        case OPC_MOV:   alu->op_type=ALU_OP_LOGICAL;    /* no carry out */
                        result = alu->b;
                        break;

        case OPC_MVN:   alu->op_type=ALU_OP_LOGICAL;    /* no carry out */
                        result = ~alu->b; 
                        break;

        case OPC_TEQ:   alu->op_type=ALU_OP_LOGICAL;    /* no carry out */
                        result = alu->a ^ alu->b; 
                        break;

        case OPC_TST:   alu->op_type=ALU_OP_LOGICAL;    /* no carry out */
                        result = alu->a & alu->b; 
                        break;

   }


	/* FLAGS p. A4-66 */
	alu->flags=0;
	alu->flags|=(result & N_FLAG);

	/* if 0, make 0 into 0xFFF..., then & with Z_FLAG to set the bit */
	alu->flags|=((~(!result)+1) & Z_FLAG);


	if (alu->op_type==ALU_OP_ARITHMETIC)
	{
		alu->flags|=(carry_out & 0x1) << C_FLAG_OFFSET;
		alu->flags|=(overflow & 0x1) << V_FLAG_OFFSET;
	}
	else
		alu->flags|=(shifter_carry_out & 0x1) << C_FLAG_OFFSET;

    return result;
}
