#ifndef ALU_H
#define ALU_H

typedef enum TALUOp
{
	ALU_OP_UNDEFINED,
	ALU_OP_LOGICAL,
	ALU_OP_ARITHMETIC
} ALUOp;

typedef struct TALU
{
	unsigned op;

	ALUOp op_type;

	unsigned a;
	unsigned b;

	unsigned logical;
	unsigned arithm;

	unsigned flags;
} ALU;

ALU* ALU_new();
unsigned ALU_exec(ALU* alu, unsigned carry_in, unsigned shifter_carry_out);

#endif

