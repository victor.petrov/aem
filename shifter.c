#include "shifter.h"

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "arm.h"
#include "util.h"

Shifter* Shifter_new()
{
    Shifter* result=NULL;
    
    result=malloc(sizeof(*result));
    if (!result)
        memerror(sizeof(*result),errno);

    return result;
}

/* Performs a shift or rotate, or passes the immediate through */
unsigned Shifter_exec(Shifter* S, unsigned carry_in)
{
    unsigned result=0;

    assert(S);

    switch (S->op)
    {
        case LSL:  result=S->value << S->amount;

                   if (S->amount>ARM_WORD_BITS)
                       S->carry_out=0;
                   else
                   if (!S->amount)
                       S->carry_out=carry_in;
                   else
                       /* last bit shifted out */
                       S->carry_out=(S->value >> (ARM_WORD_BITS - S->amount)) & BIT(0);
                   break;

        case LSR:  /* p. A5-11,12 */
                   result=S->value >> S->amount;

                    if (!S->amount)
                        S->carry_out=carry_in;
                    if (S->amount>ARM_WORD_BITS)
                        S->carry_out=0;
                    else
                        S->carry_out=(S->value >> (S->amount-1)) & BIT(0);

                    break;
        case ASR:   /* A signed shift right *should* be the same as ASR */
                    result=(unsigned)((signed)S->value >> S->amount);

                    if (!S->amount)
                        S->carry_out=MSB(S->value);
                    else
                    if (S->amount>ARM_WORD_BITS)
                        S->carry_out=0;
                    else
                        S->carry_out=(S->value >> (S->amount-1)) & BIT(0);

                    break;

        case ROR:   
                    if (S->amount)
                    {
                        result=rotater(S->value,S->amount);

                        if (S->type==SHIFT_IMM)
                            S->carry_out=MSB(result);
                        else
                            /* last bit rotated out */
                            S->carry_out=(S->value >> (S->amount - 1)) & BIT(0);
                    }
                    else
                    {
                        if (S->type!=SHIFT_IMM)
                        {
                            /* RRX */
                            result=(carry_in << 31) | (S->value >> 1);
                            /* the bit shifted off the right end */
                            S->carry_out=S->value & BIT(0);
                        }
                        else
                            /* IMM8 with #0 rotate */
                            result=S->value;
                    }

                    break;
    }

    if (S->cpsr_carry)
        S->carry_out=carry_in;


    return result;
}
