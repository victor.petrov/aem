#include "util.h"

#include <assert.h>
#include <ctype.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Duplicates a string (strdup) */
char* strduplicate(const char* source, size_t length)
{
    char* result=NULL;

    result=malloc(length+1);

    if (!result)
        return NULL;

    memcpy(result,source,length);
    result[length]='\0';

    return result;
}

/* Prints an error related to malloc and exits */
void memerror(size_t bytes, int err)
{
    fprintf(stderr,"failed to allocate %u bytes of memory: %s)\n",
            (unsigned)bytes,strerror(err));
    exit(EXIT_FAILURE);
}


/* Prints an error related to a file action and exits */
void fileerror(const char* file, const char* action, int err)
{
    fprintf(stderr,"%s: failed to %s file: %s)\n",file,action,strerror(err));
    exit(EXIT_FAILURE);
}

/* Trims whitespace from the beginning and end of a string
*  @param  str String to be trimmed of extra whitespace chars
*  @return void
*  @source stringutils.c:60, smsh (Assignment #5)
*  @author Victor Petrov <victor_petrov@harvard.edu>
*/
void trim(char* str)
{
    int i=0;                /* Loop var */
    int ltrim=0;            /* Characters to trim from the left */
    int rtrim=0;            /* Characters to trim from the right */
    int str_length=0;       /* Length of the string */

    assert(str!=NULL);

    /* Check the length of the string */
    str_length=strlen(str);
    /* Make sure there is some data */
    if (!str_length)
            return;

    /* Loop through all whitespace characters until we find
       a non-whitespace char */
    while (isspace(str[i++]))
            ++ltrim;

    /* If the whole string is whitespace, zerofill and return */
    if (ltrim==str_length)
    {
            memset(str, 0, str_length);     /* zerofill */
            return;
    }

    /* Loop through all whitespace chars from the right */
    i=str_length-1;
    while (isspace(str[i--]))
            ++rtrim;

    /* Remove left whitespace by shifting the non-whitespace chars left */
    if (ltrim)
    {
            /* Shift chars left */
            memmove(str, str+ltrim, str_length-ltrim-rtrim);
            /* Remove the offset from the right */
            memset(str+str_length-ltrim,0,ltrim);
    }

    /* Remove right whitespace by zero-filling the string */
    if (rtrim)
            memset(str+str_length-rtrim-ltrim,0,rtrim+ltrim);
}


/* Converts octal/dec/hex number string to unsigned int
* code needs to contain a pointer to the start of the string,
* such as '007' or 'FFFF'
*/
unsigned toUInt(const char* num, int base)
{
	unsigned long val=0;
	unsigned result=0;

    assert(num);

	val=strtoul(num,NULL,base);

	if (val>UINT_MAX)
		result=UINT_MAX;
	else
		result=(unsigned)val;

	return result;
}



