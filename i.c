#include "i.h"

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>

#include "util.h"

static const char* savestr(unsigned op, unsigned save);

/* Creates a new instruction */
I* I_new(unsigned bits)
{
    I* result=NULL;

    result=calloc(1,sizeof(I));
    if (!result)
        memerror(sizeof(I),errno);

    result->bits=bits;
    result->type=IT_UNDEFINED;

    return result;
}


void I_printBranch(unsigned loc,const I* i, unsigned link)
{
    assert(i);

    printf("\033[0;32m%04x:  %08x    b%s%s\t0x%x\033[0m\n",
           loc,
           i->bits,
           link?"l":"",
           condstr(i->cond),
           i->x.b.offset);
}

void I_printMultiply(unsigned loc, const I* i,
                     unsigned accumulate, unsigned save,
                     unsigned rd, unsigned rm,
                     unsigned rs, unsigned rn)
{
    assert(i);

    printf("\033[0;32m%04x:  %08x    %s%s%s\tr%02u, r%02u, r%02u",
           loc,
           i->bits, 
           accumulate?"mla":"mul",
           condstr(i->cond),
           savestr(OPC_MUL,save),
           rd,rs,rm);

    if (accumulate)
        printf(", r%02u",rn);

    printf("\033[0m\n");
}


const char* condstr(unsigned cond)
{

    switch (cond)
    {
        case COND_EQ: return "eq";
        case COND_NE: return "ne";
        case COND_CS: return "cs";
        case COND_CC: return "cc";
        case COND_MI: return "mi";
        case COND_PL: return "pl";
        case COND_VS: return "vs";
        case COND_VC: return "vc";
        case COND_HI: return "hi";
        case COND_LS: return "ls";
        case COND_GE: return "ge";
        case COND_LT: return "lt";
        case COND_GT: return "gt";
        case COND_LE: return "le";
        case COND_AL: return "";    /* omit if 'al' */
        case COND_NV: return "nv";
    }

    return "??";
}

const char* opstr(unsigned op)
{
    switch (op)
    {
        case OPC_AND: return "and";
        case OPC_EOR: return "eor";
        case OPC_SUB: return "sub";
        case OPC_RSB: return "rsb";
        case OPC_ADD: return "add";
        case OPC_ADC: return "adc";
        case OPC_SBC: return "sbc";
        case OPC_RSC: return "rsc";
        case OPC_TST: return "tst";
        case OPC_TEQ: return "teq";
        case OPC_CMP: return "cmp";
        case OPC_CMN: return "cmn";
        case OPC_ORR: return "orr";
        case OPC_MOV: return "mov";
        case OPC_BIC: return "bic";
        case OPC_MVN: return "mvn";
    }

    return "??";
}

const char* shiftstr(unsigned op)
{
    switch (op)
    {
        case LSL:   return "LSL";
        case LSR:   return "LSR";
        case ASR:   return "ASR";
        case ROR:   return "ROR";
    }

    return "???";
}

const char* savestr(unsigned op, unsigned save)
{
    if ((op==OPC_CMN) || (op==OPC_CMP) || (op==OPC_TST) || (op==OPC_TEQ) ||
        !save)
        return "";

    return "s";
}

void I_printOp(unsigned loc, const I* i, unsigned op, unsigned save, unsigned rd, unsigned rn)
{
    assert(i);

    printf("\033[0;32m%04x: %08x    %s%s%s\t",
               loc,
               i->bits, 
               opstr(op),
               condstr(i->cond),
               savestr(op,save));

    /* omit Rd */
    if (op!=OPC_CMN && op!=OPC_CMP && op!=OPC_TEQ && op!=OPC_TST)
        printf("r%u, ",rd);

    /* omit Rn */
    if (op!=OPC_MOV && op!=OPC_MVN)
        printf("r%u, ",rn);

}

void I_printMode1Immediate(unsigned immediate)
{
    printf("#0x%x\033[0m\n",immediate);
}

void I_printMode1Reg(unsigned rm)
{
    printf("r%u\033[0m\n",rm);
}

void I_printMode1RegShiftReg(unsigned rm, unsigned shift_op, unsigned rs)
{
    printf("r%u %s r%u\033[0m\n",rm,shiftstr(shift_op),rs);
}
void I_printMode1RegShiftImm(unsigned rm, unsigned shift_op, unsigned imm)
{
    if (!imm && (shift_op==ROR))
        printf("r%u, RRX\033[0m\n",rm);
    else
        printf("r%u, %s #%u\033[0m\n",rm,shiftstr(shift_op),imm);
}


