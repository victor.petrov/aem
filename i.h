#ifndef I_H
#define I_H

#include "arm.h"

/* Instruction type */
typedef enum TInstructionType
{
    IT_DATA=0,                    /* data processing instruction */
    IT_LOADSTORE=1,               /* a load or store instruciton */
    IT_BRANCH=2,                  /* a branch instruction */
    IT_UNDEFINED=3                /* an undefined instruction */
} IType;

/* Instruction condition */
typedef enum TICond
{
    COND_EQ=0x0,
    COND_NE=0x1 << (ARM_WIDTH - 4),
    COND_CS=0x2 << (ARM_WIDTH - 4),
    COND_CC=0x3 << (ARM_WIDTH - 4),
    COND_MI=0x4 << (ARM_WIDTH - 4),
    COND_PL=0x5 << (ARM_WIDTH - 4),
    COND_VS=0x6 << (ARM_WIDTH - 4),
    COND_VC=0x7 << (ARM_WIDTH - 4),
    COND_HI=0x8 << (ARM_WIDTH - 4),
    COND_LS=0x9 << (ARM_WIDTH - 4),
    COND_GE=0xA << (ARM_WIDTH - 4),
    COND_LT=0xB << (ARM_WIDTH - 4),
    COND_GT=0xC << (ARM_WIDTH - 4),
    COND_LE=0xD << (ARM_WIDTH - 4),
    COND_AL=0xE << (ARM_WIDTH - 4),
    COND_NV=0xF << (ARM_WIDTH - 4)
} ICond;

typedef struct TBranch
{
    unsigned link;
    unsigned offset;
} IBranch;

typedef struct TData
{
    unsigned immediate;
} IData;

typedef struct TMul
{
    unsigned immediate;
    unsigned accumulate;
} IMul;

typedef struct TLoadStore
{
    unsigned immediate;
} ILoadStore;


typedef struct TInstruction
{
    unsigned    bits;           /* bits read from memory */
    IType       type;           /* instruction type */
    ICond       cond;           /* instruction condition */
    const char* condstr;        /* the condition string ("eq","cs",etc) */

    union
    {
        IBranch     b;          /* branch */
        IData       d;          /* data processing */
        IMul        m;          /* multiply */
        ILoadStore ls;          /* load/store */

    } x;                        /* x for 'using x in identifiers is cool' */
} I;


/* Creates a new instruction */
I* I_new();
void I_printBranch(unsigned loc,const I* i,unsigned link);
const char* condstr(unsigned cond);
void I_printMultiply(unsigned loc, const I* i,
                     unsigned accumulate, unsigned save,
                     unsigned rd, unsigned rm,
                     unsigned rs, unsigned rn);

void I_printDataImmediate(unsigned loc,const I* i,unsigned op, unsigned save,
                          unsigned immediate, unsigned rd,unsigned rn);

void I_printDataReg(unsigned loc,const I* i,unsigned op, unsigned save,
                          unsigned rd,unsigned rn, unsigned rm);

void I_printDataRegShiftImm(unsigned loc,const I* i,unsigned op, unsigned save,
                              unsigned shift_op, unsigned shift_amount, 
                              unsigned rd, unsigned rn, unsigned rm);

void I_printDataRegShiftReg(unsigned loc,const I* i,unsigned op, unsigned save,
                              unsigned shift_op, 
                              unsigned rd, unsigned rn, unsigned rs, unsigned rm);


void I_printOp(unsigned loc, const I* i, unsigned op, unsigned save, unsigned rd, unsigned rn);
void I_printMode1Immediate(unsigned immediate);
void I_printMode1Reg(unsigned rm);
void I_printMode1RegShiftReg(unsigned rm, unsigned shift_op, unsigned rs);
void I_printMode1RegShiftImm(unsigned rm, unsigned shift_op, unsigned imm);

#endif

