#include "mif.h"
#include "util.h"

#include <assert.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MIF_BUF_GROW_BY 1024
#define MIF_LINE_MAXLEN 50
#define MIF_SEPARATORS          " \t\r\n"
#define MIF_SEPARATORS_EOL      " \t\r\n;"
#define MIF_SEPARATORS_RANGE    " \t[.];"

/** Makes sure the next token is the one expected
* @param MIF*  m : The MIF struct
* @param char* y : Expected token
* @warning exits on Failure
*/
#define EXPECT(m,y) {                                                         \
                      char* x=strtok(NULL,MIF_SEPARATORS);                    \
                      if (!x)                                                 \
                      {                                                       \
                          fprintf(stderr,"%s:%u: unexpected end of line.",    \
                                  m->filename,m->nlines);                     \
                          exit(EXIT_FAILURE);                                 \
                      }                                                       \
                      if (!strequals(x,y))                                    \
                      {                                                       \
                          fprintf(stderr,"%s:%u: unexpected token '%s', "     \
                                  "expecting '%s'.\n",m->filename,m->nlines,  \
                                  x,y);                                       \
                          exit(EXIT_FAILURE);                                 \
                      }                                                       \
                    }

/** Calls MIF_set_conf with the next token returned by strtok()
* @param MIF*   x : The MIF struct
* @param MIFKey y : The MIF config key
*/
#define MIF_CONF(x,y)   { MIF_set_conf((x),(y),strtok(NULL,MIF_SEPARATORS)); }

static unsigned MIF_parse_confline(MIF* mif, char* f);
static unsigned MIF_parse_dataline(MIF* mif, char* f);
static unsigned MIF_parse_range(MIF* mif, char* range);
static void MIF_set_conf(MIF* mif,MIFKey key,char* value);
static void MIF_validateValue(const MIF* mif,const char* token,MIFRadix radix);
static void MIF_set_value(MIF* mif, unsigned location, unsigned value);

MIF* MIF_load(const char* file)
{
    /*unsigned bytes_read=0;*/
    unsigned data_section=0;

    FILE* mif=NULL;
    char* line;
    MIF* result=NULL;

    assert(file);

    line=calloc(MIF_LINE_MAXLEN,sizeof(*line));

    /* open the file */
    mif=fopen(file,"r");
    if (!mif)
        fileerror(file,"open",errno);

    result=MIF_new(file);

    /* read file data until we reach the content section */
    while (!feof(mif) && !data_section)
    {
        /* read one line */
        if (fgets(line,MIF_LINE_MAXLEN,mif))
        {
            result->nlines++;
            data_section=MIF_parse_confline(result,line);
        }
    }

    /* allocate an array large enough to hold the data.
       the size is specified as the DEPTH in the MIF config */
    result->data=calloc(result->depth,result->width);

    if (!result->data)
        memerror(result->depth,sizeof(*(result->data)));

    /* read the data section */
    while (!feof(mif) && data_section)
    {
        /* read one line */
        if (fgets(line,MIF_LINE_MAXLEN,mif))
        {
            result->nlines++;
            data_section=!MIF_parse_dataline(result,line);
        }
    }

    fclose(mif);

    return result;
}

/* Initialize memory for a new MIF struct */
MIF* MIF_new(const char* file)
{
    MIF* result=NULL;

    /* allocate memory for the MIF struct */
    result=calloc(1,sizeof(*result));

    if (!result)
        memerror(sizeof(*result),errno);

    if (file)
        result->filename=strduplicate(file,strlen(file));

    /* default radii */
    result->addr_radix=MIF_HEX;
    result->data_radix=MIF_HEX;

    return result;
}

/** Parses a line from the configuration header
* @return 1 if the data section has been reached or 0 otherwise
*/
unsigned MIF_parse_confline(MIF* mif, char* line)
{   
    char* tok=NULL;
    static unsigned comment_block=0;

    assert(mif);
    assert(line);

    trim(line);

    tok=strtok(line,MIF_SEPARATORS);

    /* % comment */
    if (strchr(tok,'%'))
    {
        comment_block=!comment_block;

        if (!comment_block)
            return 0;

        /* check for closing % on the same line */
        while ((tok=strtok(NULL,MIF_SEPARATORS)))
        {
            /* found % on the same line? */
            if (strchr(tok,'%'))
            {
                comment_block=0;
                return 0;
            }
            else
            /* found EOL? */
            if (strequals(tok,"\n") || strequals(tok,"\r"))
                return 0;
        }

        return 0;
    }
    else
        if (comment_block)
            return 0;

    /* skip empty lines */
    if (!strlen(tok))
        return 0;

    /* -- comment */
    if (strequals(tok,"--"))
        return 0;

    /* newline: skip */
    if (strequals(tok,"\n") || strequals(tok,"\r"))
        return 0;

    /* CONTENT */
    if (strequals(tok,"CONTENT"))
        return 0;

    /* BEGIN */
    if (strequals(tok,"BEGIN"))
        return 1;

    /* DEPTH */
    if (strequals(tok,"DEPTH"))
    {
        EXPECT(mif,"=");
        MIF_CONF(mif,MIF_DEPTH);
    } 
    else
    /* WIDTH */
    if (strequals(tok,"WIDTH"))
    {
        EXPECT(mif,"=");
        MIF_CONF(mif,MIF_WIDTH);
    }
    else
    /* ADDR_RADIX */
    if (strequals(tok,"ADDRESS_RADIX"))
    {
        EXPECT(mif,"=");
        MIF_CONF(mif,MIF_ADDRESS_RADIX);
    }
    /* DATA_RADIX */
    else
    if (strequals(tok,"DATA_RADIX"))
    {
        EXPECT(mif,"=");
        MIF_CONF(mif,MIF_DATA_RADIX);
    }
    else
    {
        fprintf(stderr,"%s:%d: unknown configuration option '%s'\n",
                mif->filename,mif->nlines,tok);
        exit(EXIT_FAILURE);
    }

    return 0;
}

/** Parses a line from the configuration header
* @return 1 if the data section has ended or 0 otherwise
*/
unsigned MIF_parse_dataline(MIF* mif, char* line)
{   
    char* tok=NULL;
    static unsigned comment_block=0;
    unsigned toklen=0;
    unsigned address=0;
    unsigned data=0;

    assert(mif);
    assert(line);

    trim(line);

    if (!strlen(line))
        return 0;

    /* [xx .. yy] ? */
    if (line[0]=='[')
        return MIF_parse_range(mif, line);

    tok=strtok(line,MIF_SEPARATORS);

    if (!tok)
        return 0;

    toklen=strlen(tok);

    /* skip empty lines */
    if (!strlen(tok))
        return 0;

    /* % comment */
    if (strchr(tok,'%'))
    {
        comment_block=!comment_block;

        if (!comment_block)
            return 0;

        /* check for closing % on the same line */
        while ((tok=strtok(NULL,MIF_SEPARATORS)))
        {
            /* found % on the same line? */
            if (strchr(tok,'%'))
            {
                comment_block=0;
                return 0;
            }
            else
            /* found EOL? */
            if (strequals(tok,"\n") || strequals(tok,"\r"))
                return 0;
        }

        return 0;
    }
    else
        if (comment_block)
            return 0;

    /* -- comment */
    if (strequals(tok,"--"))
        return 0;

    /* newline: skip */
    if (strequals(tok,"\n") || strequals(tok,"\r"))
        return 0;

    /* END */
    if (strstr(tok,"END")==tok)
        return 1;

    /* validate that the address is correct, according
       to the radix chosen */
    MIF_validateValue(mif,tok,mif->addr_radix);

    /* convert the address to a number */
    address=toUInt(tok,mif->addr_radix);

    /* the address must be followed by a ':' */
    EXPECT(mif,":");

    /* separate the adress value from the trailing ';' */
    tok=strtok(NULL,MIF_SEPARATORS_EOL);

    if (!tok || !strlen(tok))
    {
        fprintf(stderr,"%s:%d: missing address value",mif->filename,
                mif->nlines);
        exit(EXIT_FAILURE);
    }

    /* validate the address value */
    MIF_validateValue(mif,tok,mif->data_radix);

    /* convert the address value to unsigned int */
    data=toUInt(tok,mif->data_radix);

    if (address > mif->depth)
    {
        fprintf(stderr,"%s:%d: address exceeds DEPTH value (%u)\n",
                mif->filename,mif->nlines,mif->depth);
        exit(EXIT_FAILURE);
    }

    /* write the data value at the address location */
    MIF_set_value(mif,address,data);

    return 0;
}

/* parses a MIF range line: [XX..YY] : ZZ; */
unsigned MIF_parse_range(MIF* mif, char* range)
{
    char* token=NULL;
    unsigned address1=0;
    unsigned address2=0;
    unsigned data=0;
    unsigned i=0;

    assert(mif);
    assert(range);

    /* tokenize the range string */
    token=strtok(range,MIF_SEPARATORS_RANGE);

    if (!token || !strlen(token))
    {
        fprintf(stderr,"%s:%d: missing range address",mif->filename,
                mif->nlines);
        exit(EXIT_FAILURE);
    }

    /* validate the string */
    MIF_validateValue(mif,token,mif->addr_radix);

    address1=toUInt(token,mif->addr_radix);

    /* read second address */
    token=strtok(NULL,MIF_SEPARATORS_RANGE);

    if (!token || !strlen(token))
    {
        fprintf(stderr,"%s:%d: missing range address",mif->filename,
                mif->nlines);
        exit(EXIT_FAILURE);
    }

    /* validate the string */
    MIF_validateValue(mif,token,mif->addr_radix);

    address2=toUInt(token,mif->addr_radix);

    /* read data value */
    token=strtok(NULL,MIF_SEPARATORS_RANGE);

    if (!token || !strlen(token))
    {
        fprintf(stderr,"%s:%d: missing range value",mif->filename,
                mif->nlines);
        exit(EXIT_FAILURE);
    }

    /* validate the string */
    MIF_validateValue(mif,token,mif->data_radix);

    /* convert string to unsigned integer */
    data=toUInt(token,mif->data_radix);

    if ((address1 > mif->depth) || (address2 > mif->depth))
    {
        fprintf(stderr,"%s:%d: address range exceeds DEPTH (%u)\n",
                mif->filename,mif->nlines,mif->depth);
        exit(EXIT_FAILURE);
    }

    if (address1 > address2)
    {
        fprintf(stderr,"%s:%d: range end address must be greater than the "
                "starting address\n",mif->filename,mif->nlines);
        exit(EXIT_FAILURE);
    }

    /* loop and set all values */
    for(i=address1;i<=address2;++i)
        MIF_set_value(mif,i,data);

    return 0;
}


void MIF_set_conf(MIF* mif,MIFKey key,char* value)
{
    assert(mif);

    value=strtok(value,";");

    if (!value)
    {
        fprintf(stderr,"%s:%u: missing value\n",mif->filename,mif->nlines);
        exit(EXIT_FAILURE);
    }

    switch (key)
    {
        case MIF_DEPTH: mif->depth=toUInt(value,10); break;
        case MIF_WIDTH: mif->width=toUInt(value,10); 
                        if ((mif->width!=8) && (mif->width!=16) && 
                            (mif->width!=32))
                        {
                            fprintf(stderr,"%s:%d: WIDTH value must be one of "
                                    "8, 16 or 32.\n",mif->filename,
                                    mif->nlines);
                            exit(EXIT_FAILURE);
                        }
                        break;
        case MIF_ADDRESS_RADIX: /* BIN */
                                if (strequals(value,"BIN"))
                                    mif->addr_radix=2;
                                else
                                /* OCT */
                                if (strequals(value,"OCT"))
                                    mif->addr_radix=8;
                                else
                                /* DEC */
                                if (strequals(value,"DEC"))
                                {
                                    mif->addr_radix=10;
                                    fprintf(stderr,"%s:%d: warning: DEC "
                                    "addresses will be cast to UNS\n",
                                    mif->filename,mif->nlines);
                                }
                                else
                                /* UNS */
                                if (strequals(value,"UNS"))
                                    mif->addr_radix=10;
                                else
                                /* HEX */
                                if (strequals(value,"HEX"))
                                    mif->addr_radix=16;
                                else
                                {
                                    fprintf(stderr,"%s:%d: error: unknown "
                                            "radix '%s'\n",mif->filename,
                                            mif->nlines,value);
                                    exit(EXIT_FAILURE);
                                }

                                break;
        case MIF_DATA_RADIX:    /* BIN */
                                if (strequals(value,"BIN"))
                                    mif->data_radix=2;
                                else
                                /* OCT */
                                if (strequals(value,"OCT"))
                                    mif->data_radix=8;
                                else
                                /* DEC */
                                if (strequals(value,"DEC"))
                                {
                                    mif->data_radix=10;
                                    fprintf(stderr,"%s:%d: warning: DEC "
                                    "data will be cast to UNS\n",
                                    mif->filename,mif->nlines);
                                }
                                else
                                /* UNS */
                                if (strequals(value,"UNS"))
                                    mif->data_radix=10;
                                else
                                /* HEX */
                                if (strequals(value,"HEX"))
                                    mif->data_radix=16;
                                else
                                {
                                    fprintf(stderr,"%s:%d: error: unknown "
                                            "radix '%s'\n",mif->filename,
                                            mif->nlines,value);
                                    exit(EXIT_FAILURE);
                                }

                                break;

        default: fprintf(stderr,"%s:%u: invalid configuration option\n",
                         mif->filename,mif->nlines);
    }
}

/* Validates the loaded MIF configuration and data */
void MIF_validate(const MIF* mif)
{
    assert(mif);

    /* WIDTH */
    if (!mif->width)
    {
        fprintf(stderr,"%s: no WIDTH configuration option found\n",
                mif->filename);
        exit(EXIT_FAILURE);
    }

    /* DEPTH */
    if (!mif->depth)
    {
        fprintf(stderr,"%s: no DEPTH configuration option found\n",
                mif->filename);
        exit(EXIT_FAILURE);
    }

    /* ADDR_RADIX */
    if (!mif->addr_radix)
    {
        fprintf(stderr,"%s: no ADDRESS_RADIX configuration option found\n",
                mif->filename);
        exit(EXIT_FAILURE);
    }

    /* DATA_RADIX */
    if (!mif->data_radix)
    {
        fprintf(stderr,"%s: no DATA_RADIX configuration option found\n",
                mif->filename);
        exit(EXIT_FAILURE);
    }

    /* DATA and SIZE */
    if (!mif->data)
    {
        fprintf(stderr,"%s: no data found\n",mif->filename);
        exit(EXIT_FAILURE);
    }
}

void MIF_validateValue(const MIF* mif, const char* token, MIFRadix radix)
{
    unsigned toklen =0;

    assert(token);

    toklen=strlen(token);
/* TODO: The isalnum() functions should instead be isdigit() and they should
   iterate over each character
    switch (radix)
    {
        case MIF_BIN: if (toklen!=strspn(token,"01"))
                            {
                                fprintf(stderr,"%s:%d: binary value '%s' "
                                        "cointains invalid characters\n",
                                        mif->filename,mif->nlines,token);
                                exit(EXIT_FAILURE);
                            }
                            break;

        case MIF_OCT: if (toklen!=strspn(token,"01234567"))
                            {
                                fprintf(stderr,"%s:%d: octal value '%s' "
                                        "contains invalid characters\n",
                                        mif->filename,mif->nlines,token);
                                exit(EXIT_FAILURE);
                            }
                            break;

        case MIF_DEC: if (!isalnum(token))
                            {
                                fprintf(stderr,"%s:%d: decimal value '%s' "
                                        "contains non-numeric characters\n",
                                        mif->filename,mif->nlines,token);
                                exit(EXIT_FAILURE);
                            }
                            break;

        case MIF_UNS: if (!isalnum(token))
                            {
                                fprintf(stderr,"%s:%d: unsigned value '%s' "
                                        "contains non-numeric characters\n",
                                        mif->filename,mif->nlines,token);
                                exit(EXIT_FAILURE);
                            }
                            break;
        case MIF_HEX: if (!isxdigit(token))
                            {
                                fprintf(stderr,"%s:%d: hexadecimal value "
                                        "'%s' contains invalid characters\n",
                                        mif->filename,mif->nlines,token);
                                exit(EXIT_FAILURE);
                            }
                            break;
    }
    */
}

/* writes a value to the MIF data array, based on the WIDTH */
static void MIF_set_value(MIF* mif, unsigned location, unsigned value)
{
    unsigned* words=0;
    unsigned short* halfwords=0;

    assert(mif);
    assert(mif->data);

    words=(unsigned*)(mif->data);
    halfwords=(unsigned short*)(mif->data);

    switch (mif->width)
    {
        case  8: mif->data[location]=(unsigned char)value;break;
        case 16: halfwords[location]=(unsigned short)value;break;
        case 32: words[location]=value;break;
    }
}
