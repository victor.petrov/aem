#include "mul.h"

#include <assert.h>
#include <errno.h>
#include <stdlib.h>

#include "arm.h"
#include "util.h"

MUL* MUL_new()
{
	MUL* result=NULL;

	result=malloc(sizeof(*result));
	if (!result)
		memerror(sizeof(*result),errno);

	return result;
}

unsigned MUL_exec(MUL* mul)
{
	unsigned result=0;

	assert(mul);

	result = mul->a * mul->b;
	
	/* flags */
	mul->flags=0;

	mul->flags|=result & N_FLAG;
	mul->flags|=((!(!result))-1) & Z_FLAG;

	/* FIXME: N and C flags */


	return result;
}

