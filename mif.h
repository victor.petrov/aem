#ifndef MIF_H
#define MIF_H

#include <assert.h>
#include <stdlib.h>

/* valid MIF radix values */
typedef enum TMIFRadix
{
    MIF_BIN,
    MIF_OCT,
    MIF_DEC,
    MIF_UNS,
    MIF_HEX
} MIFRadix;

/* MIF configuration keys */
typedef enum TMIFKey
{
    MIF_DEPTH,
    MIF_WIDTH,
    MIF_ADDRESS_RADIX,
    MIF_DATA_RADIX
} MIFKey;

/* Container for the MIF file */
typedef struct TMIF
{
    char* filename;         /* file path */
    unsigned nlines;        /* number of lines read */
    unsigned char* data;    /* data read */
    MIFRadix addr_radix;    /* address radix */
    MIFRadix data_radix;    /* data radix */
    unsigned depth;         /* depth */
    unsigned width;         /* width */
} MIF;

/* Reads a MIF file and returns an initialzed array and its size */
MIF* MIF_load(const char* file);

/* Instantiates a new MIF struct and sets the filename property */
MIF* MIF_new(const char* file);

/* Validates the loaded MIF configuration and data */
void MIF_validate(const MIF* mif);
#endif

